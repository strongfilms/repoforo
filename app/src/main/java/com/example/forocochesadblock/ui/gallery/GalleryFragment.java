package com.example.forocochesadblock.ui.gallery;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.webkit.WebSettingsCompat;

import com.example.forocochesadblock.MainActivity;
import com.example.forocochesadblock.R;
import com.example.forocochesadblock.ui.home.HomeFragment;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private String mainUrl = "https://m.forocoches.com/foro/forumdisplay.php?f=2";
    private String logoutUrl = "https://m.forocoches.com/foro/login.php?do=logout&logouthash=1609287661-8c2df69320407f0d917dc2df140f25a383d4644e";
    private WebView web;
    private Button button;
    private LinearLayout changecolor;
    public static Switch darkmode, keepPage;
    private LinearLayout redBox;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        Log.i("tags", "Ajustes tab");

        darkmode = root.findViewById(R.id.switchdark);
        keepPage = root.findViewById(R.id.switchkeepPage);

        if(MainActivity.darkmode == true){
            darkmode.setChecked(true);
        }else{
            darkmode.setChecked(false);
        }

        if(MainActivity.keepPage == true){
            keepPage.setChecked(true);
        }else{
            keepPage.setChecked(false);
        }

        keepPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(keepPage.isChecked()){
                    MainActivity.keepPage = true;
                    Log.i("tags", "setTrue");
                }else{
                    MainActivity.keepPage = false;
                    Log.i("tags", "setFalse");
                }
            }
        });

//        changecolor = root.findViewById(R.id.changecolors);
//
//        changecolor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) { //Esto tiene que abrir el color picker
//
//                LayoutInflater inflater = getLayoutInflater();
//                View view = inflater.inflate(R.layout.colorpicker, null);
//
//                LinearLayout redSelected = (LinearLayout) view.findViewById(R.id.redselected);
//                LinearLayout blueSelected = (LinearLayout) view.findViewById(R.id.blueselected);
//                LinearLayout purpleSelected = (LinearLayout) view.findViewById(R.id.purpleselected);
//                LinearLayout greenSelected = (LinearLayout) view.findViewById(R.id.greenselected);
//                LinearLayout darkgreenSelected = (LinearLayout) view.findViewById(R.id.darkgreenselected);
//
//                if(view.getParent() != null) {
//                    ((ViewGroup)view.getParent()).removeView(view);
//                }
//                MainActivity.builder.setView(view);
//
//                MainActivity.alertDialog = MainActivity.builder.create();
//
//                MainActivity.alertDialog.setCancelable(false);
//
//                MainActivity.alertDialog.show();
//
//                purpleSelected.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MainActivity.alertDialog.dismiss();
//                                MainActivity.lastcolor = MainActivity.color;
//                                MainActivity.color = R.color.purple_700;
//                                MainActivity.theme = R.style.Theme_ForocochesAdBlock;
//                                MainActivity.flag = 2;
//
//                                MainActivity.reload.recreate();
//                            }
//                        }
//                );
//
//                redSelected.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MainActivity.alertDialog.dismiss();
//                                MainActivity.lastcolor = MainActivity.color;
//                                MainActivity.color = R.color.rojo;
//                                MainActivity.theme = R.style.Theme_ForoRojo;
//                                MainActivity.flag = 2;
//
//                                MainActivity.reload.recreate();
//                            }
//                        }
//                );
//
//                blueSelected.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MainActivity.alertDialog.dismiss();
//                                MainActivity.lastcolor = MainActivity.color;
//                                MainActivity.color = R.color.blue_700;
//                                MainActivity.theme = R.style.Theme_ForoAzul;
//                                MainActivity.flag = 3;
//
//                                MainActivity.reload.recreate();
//                            }
//                        }
//                );
//
//                greenSelected.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MainActivity.alertDialog.dismiss();
//                                MainActivity.lastcolor = MainActivity.color;
//                                MainActivity.color = R.color.green_700;
//                                MainActivity.theme = R.style.Theme_ForoVerde;
//                                MainActivity.flag = 3;
//
//                                MainActivity.reload.recreate();
//                            }
//                        }
//                );
//
//                darkgreenSelected.setOnClickListener(
//                        new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                MainActivity.alertDialog.dismiss();
//                                MainActivity.lastcolor = MainActivity.color;
//                                MainActivity.color = R.color.dark_green_700;
//                                MainActivity.theme = R.style.Theme_ForoVerdeOscuro;
//                                MainActivity.flag = 3;
//
//                                MainActivity.reload.recreate();
//                            }
//                        }
//                );
//
//            }
//        });

        return root;

    }

    @Override
    public void onPause() {
        super.onPause();
        if(darkmode.isChecked()){
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
            MainActivity.darkmode = true;
        }else{
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_OFF);
            MainActivity.darkmode = false;
        }

        if(keepPage.isChecked()){
            MainActivity.keepPage = true;
            HomeFragment.keepPage = true;
        }else{
            MainActivity.keepPage = false;
            HomeFragment.keepPage = true;
        }

    }
}