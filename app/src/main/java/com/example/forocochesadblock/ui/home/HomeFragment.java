package com.example.forocochesadblock.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.example.forocochesadblock.MainActivity;
import com.example.forocochesadblock.R;
import com.example.forocochesadblock.ui.gallery.GalleryFragment;
import com.example.forocochesadblock.ui.slideshow.SlideshowFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    public static String mainUrl = "https://m.forocoches.com/foro/forumdisplay.php?f=2";
    public static WebView web;
    public boolean darkMode = MainActivity.darkmode;
    public static boolean keepPage;
    public String lastUrl = MainActivity.lastUrl;
    public static AdView mAdView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //hola

        keepPage = MainActivity.keepPage;

        Log.i("tags", "Cambio a Foro");
        Log.i("tags", "keepPage: "+keepPage);

        web = (WebView) root.findViewById(R.id.webView);

        if(keepPage == true){

            if(MainActivity.traigoUrl == true){
                MainActivity.urlToLoad = MainActivity.urlTraida;
                MainActivity.traigoUrl = false;
            }

        }else{

            if(MainActivity.traigoUrl == true){

                MainActivity.urlToLoad = MainActivity.urlTraida;
                MainActivity.traigoUrl = false;

            }else {
                MainActivity.urlToLoad = mainUrl;
            }
        }

        cargarAnuncio();

        web.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }
        });

        web.canGoBack();
        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        //settings.setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/cache");
        //settings.setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/databases");

        if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
            if(darkMode == true) {
                WebSettingsCompat.setForceDark(web.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                HomeFragment.web.reload();
            }else{
                WebSettingsCompat.setForceDark(web.getSettings(), WebSettingsCompat.FORCE_DARK_OFF);
                HomeFragment.web.reload();
            }
        }

        web.setWebViewClient(new MyWebViewClient());
        MyWebChromeClient myWebChromeClient = new MyWebChromeClient();
        web.setWebChromeClient(myWebChromeClient);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            web.setForceDarkAllowed(true);
        }

        web.loadUrl(MainActivity.urlToLoad);

        MainActivity.hilo = new Thread(MainActivity.runnable);
        MainActivity.hilo.start();
        //checkThreadUpdates("https://m.forocoches.com/foro/showthread.php?t=8164530&highlight=");
        MainActivity.cookies = CookieManager.getInstance().getCookie(web.getUrl());

        //Log.i("tags", MainActivity.cookies);

        return root;
    }

    public static void cargarAnuncio(){
    };

    private class MyWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView web, String mainUrl){
            web.loadUrl(mainUrl);
            return true;
        }
    }

    public class MyWebChromeClient extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result)
        {
            final JsResult finalRes = result;
            new AlertDialog.Builder(view.getContext())
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new AlertDialog.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finalRes.confirm();
                                }
                            })
                    .setCancelable(false)
                    .create()
                    .show();
            return true;
        }
    }

    public void checkThreadUpdates(String threadUrl){

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Document doc = Jsoup.connect(threadUrl)
                            .data("query", "Java")
                            .userAgent("Mozilla")
                            .cookie("auth", "token")
                            .timeout(3000)
                            .post();

                    Log.i("tags", doc.title());

                    Log.i("tags", "Url: " + threadUrl);

                    Element element = doc.selectFirst("em");
                    Log.i("tags", "Length: " + element.text());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}