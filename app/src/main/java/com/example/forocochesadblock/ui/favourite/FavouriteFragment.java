package com.example.forocochesadblock.ui.favourite;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.example.forocochesadblock.MainActivity;
import com.example.forocochesadblock.R;
import com.example.forocochesadblock.bd.Database;
import com.example.forocochesadblock.objects.AdapterPers;
import com.example.forocochesadblock.objects.Hilo;
import com.example.forocochesadblock.ui.home.HomeFragment;
import com.example.forocochesadblock.ui.home.HomeViewModel;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class FavouriteFragment extends Fragment {

    private FavouriteViewModel favouriteViewModel;
    private String id_delete;
    public static ListView listView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        favouriteViewModel =
                new ViewModelProvider(this).get(FavouriteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_favourite, container, false);

        Database.bandera = 2;
        ArrayList<Hilo> arrayHilos = MainActivity.db.readAll();

//        for(int i=0; i<arrayHilos.size(); i++){
//
//            String oldString = arrayHilos.get(i).getTitulo();
//            String newString = oldString.replace(" - Foro Coches", " ");
//            arrayHilos.get(i).setTitulo(newString);
//
//        }

        MainActivity.adapter = new AdapterPers(getContext(), arrayHilos);
        // Attach the adapter to a ListView
        listView = (ListView) root.findViewById(R.id.listaFavoritos);
        listView.setAdapter(MainActivity.adapter);

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Hilo item = (Hilo)parent.getItemAtPosition(position);

                //Toast.makeText(getContext(), item.getTitulo(), Toast.LENGTH_SHORT).show();

                Database.bandera = 1;
                ArrayList<Hilo> hilos = MainActivity.db.readAll();

                for(int i=0; i<hilos.size(); i++){

//                    String oldString = hilos.get(i).getTitulo();
//                    String newString = oldString.replace(" - Foro Coches", " ");
//                    hilos.get(i).setTitulo(newString);

                    if(hilos.get(i).getTitulo().equalsIgnoreCase(item.getTitulo())){

                        MainActivity.traigoUrl = true;
                        MainActivity.urlTraida = hilos.get(i).getUrl();

                        MainActivity.navController.navigate(R.id.nav_home);

                    }

                }

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Hilo item = (Hilo)parent.getItemAtPosition(position);

                //Toast.makeText(getContext(), item.getId(), Toast.LENGTH_SHORT).show();

                id_delete = item.getId();

                return false;
            }
        });

        return root;

    }

    public void reloadList(){

        Database.bandera = 1;
        ArrayList<Hilo> arrayHilos = MainActivity.db.readAll();

//        for(int i=0; i<arrayHilos.size(); i++){
//
//            String oldString = arrayHilos.get(i).getTitulo();
//            String newString = oldString.replace(" - Foro Coches", "");
//            arrayHilos.get(i).setTitulo(newString);
//
//        }

        MainActivity.adapter = new AdapterPers(getContext(), arrayHilos);
        listView.setAdapter(MainActivity.adapter);

    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        MainActivity.inflater.inflate(R.menu.menu_list, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        MainActivity.db.delete(id_delete);
        reloadList();

        return super.onContextItemSelected(item);
    }
}