package com.example.forocochesadblock.objects;

public class Hilo {

    private String id;
    private String titulo;
    private String url;
    private String longitud_op;

    public Hilo(String id, String titulo, String url, String longitud_op) {
        this.id = id;
        this.titulo = titulo;
        this.url = url;
        this.longitud_op = longitud_op;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLongitud_op() {
        return longitud_op;
    }

    public void setLongitud_op(String longitud_op) {
        this.longitud_op = longitud_op;
    }
}
