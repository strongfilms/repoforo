package com.example.forocochesadblock.objects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.forocochesadblock.R;

import java.util.ArrayList;

public class AdapterPers extends ArrayAdapter<Hilo> {

    public AdapterPers(Context context, ArrayList<Hilo> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Hilo hilo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.diseno_fila, parent, false);
        }
        // Lookup view for data population
        TextView nombreHilo = (TextView) convertView.findViewById(R.id.nombreHilo);
        // Populate the data into the template view using the data object
        nombreHilo.setText(hilo.getTitulo());
        // Return the completed view to render on screen
        return convertView;
    }
}
