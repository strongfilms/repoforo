package com.example.forocochesadblock.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.contentcapture.DataShareWriteAdapter;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.example.forocochesadblock.MainActivity;
import com.example.forocochesadblock.objects.Hilo;
import com.example.forocochesadblock.ui.favourite.FavouriteFragment;
import com.example.forocochesadblock.ui.home.HomeFragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "FavThreads.db";
    public static final String TABLA_NOMBRES = "hilos";
    public static final String COLUMNA_ID = "_id";
    public static final String COLUMNA_TITULO = "titulo";
    public static final String COLUMNA_URL = "url";
    public static final String LONGITUD_OP = "longitud_op";
    public static String text = null;
    public static String id_hilo;
    public static String threadUrl;
    public static int bandera;

    private static final String SQL_CREAR = "create table "
            + TABLA_NOMBRES + "(" + COLUMNA_ID
            + " integer primary key autoincrement, " + COLUMNA_TITULO
            + " text not null, " + COLUMNA_URL + " text not null, " + LONGITUD_OP + " text not null" + ");";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREAR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void agregarHilo(){

        String url = "";

        url = HomeFragment.web.getUrl();

        String op = null;

        checkThreadEdit(url);

        Thread hilo = new ThreadJoining();

        hilo.start();
        try {
            hilo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        String titulo = "";

        titulo = HomeFragment.web.getTitle();

        String nuevoTitulo = titulo.replace(" - Foro Coches","");

        Log.i("tags", "El op: " + text);

        values.put(LONGITUD_OP, text);
        values.put(COLUMNA_TITULO, nuevoTitulo);
        values.put(COLUMNA_URL, url);

        db.insert(TABLA_NOMBRES, null, values);
        db.close();

    }

    public Hilo obtener(String titulo){

        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {COLUMNA_ID, COLUMNA_TITULO, COLUMNA_URL, LONGITUD_OP};



        Cursor cursor =
                db.query(TABLA_NOMBRES,
                        projection,
                        " titulo = ?",
                        new String[] { titulo + " - Foro Coches" },
                        null,
                        null,
                        null,
                        null);


        if (cursor != null)
            cursor.moveToFirst();

        System.out.println("El nombre es " +  cursor.getString(1) );

        Hilo hilo = new Hilo(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));

        db.close();

        return hilo;

    }

    public ArrayList<Hilo> readAll(){

        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {COLUMNA_ID, COLUMNA_TITULO, COLUMNA_URL, LONGITUD_OP};

        ArrayList<Hilo> hilos = new ArrayList<>();

        Cursor cursor =
                db.query(TABLA_NOMBRES,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        null);


//        if (cursor != null)
//            cursor.moveToFirst();

        while(cursor.moveToNext()){

            hilos.add(new Hilo(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
            String estado_anterior = cursor.getString(3);
            checkThreadEdit(cursor.getString(2));
            //Log.i("Tags leer: ", estado_anterior);

            Thread hilo = new ThreadJoining();
            hilo.start();
            try {
                hilo.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(bandera == 2){

                if(compareStrings(estado_anterior, text)){
                    Log.i("tags", "No modificado");
                }else{
                    Log.i("tags", "Modificado"); //lanzar noti con url
                    id_hilo = cursor.getString(0);
                    updateRow();
                    MainActivity.nombre_hilo_actualizado = cursor.getString(1);
                    MainActivity.lanzarNoti();
                }

            }

        }

        db.close();

        //Thread hilo1 = new ThreadJoining();
        //checkThreadEdit(cursor.getString(1));

        return hilos;

    }

    public void delete(String id){

        SQLiteDatabase db = this.getReadableDatabase();
        int cuantos = db.delete(TABLA_NOMBRES, COLUMNA_ID + "=" + id, null);

        Log.i("deleted: ", String.valueOf(cuantos));

    }

    public void updateRow(){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(LONGITUD_OP, text);

        int cuantos = db.update(TABLA_NOMBRES, cv, COLUMNA_ID + "=" + id_hilo, null);
        Log.i("updated: ", String.valueOf(cuantos));

        db.close();

    }

    public boolean compareStrings(String string1, String string2){

        if(string1.equalsIgnoreCase(string2)){
            return true;
        }else{
            return false;
        }

    }

    public void checkThreadEdit(String threadUrl) {

        Database.threadUrl = threadUrl;

    }

    class ThreadJoining extends Thread {

        @Override
        public void run() {

            try {
                Document doc = Jsoup.connect(threadUrl + "&highlight=")
                        .data("query", "Java")
                        .userAgent("Mozilla")
                        .timeout(3000)
                        .post();

                //Log.i("tags", doc.title());

                //Log.i("tags", "Url: " + threadUrl + "&highlight=");

                Element element = doc.selectFirst("em");
                text = element.text();
                //Log.i("tags", "Length: " + text);

            } catch (Exception e) {
                text = "error";
                e.printStackTrace();
            }
        }

    }

}
