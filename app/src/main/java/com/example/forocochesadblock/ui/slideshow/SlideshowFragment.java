package com.example.forocochesadblock.ui.slideshow;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.webkit.WebSettingsCompat;

import com.example.forocochesadblock.MainActivity;
import com.example.forocochesadblock.R;
import com.example.forocochesadblock.ui.home.HomeFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;
    private AdView mAdView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

//        mAdView = root.findViewById(R.id.adView2);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        Log.i("tags", "Acerca de tab");

        return root;
    }

    @Override
    public void onPause() {//Esto tampoco va?
        super.onPause();
        if(MainActivity.darkmode == true){
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
            HomeFragment.web.reload();
        }else{
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_OFF);
            HomeFragment.web.reload();
        }
    }

}