package com.example.forocochesadblock;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.example.forocochesadblock.bd.Database;
import com.example.forocochesadblock.objects.AdapterPers;
import com.example.forocochesadblock.objects.Hilo;
import com.example.forocochesadblock.ui.gallery.GalleryFragment;
import com.example.forocochesadblock.ui.home.HomeFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.webkit.WebSettingsCompat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String CHANNEL_ID = "1";
    private AppBarConfiguration mAppBarConfiguration;
    public static boolean darkmode;
    public static boolean keepPage;
    public static String lastUrl = HomeFragment.mainUrl;
    public static String urlToLoad;
    public static ColorPicker colorPicker;
    public Button copyLink, sharewapp;
    private AdView mAdView;
    public static AlertDialog.Builder dialog;
    public static View view;

    //For colors
    public static LinearLayout lateralDrawer;

    public static LinearLayout redbox;

    //public static AlertDialog.Builder builder;
    //public static AlertDialog alertDialog;

    public static int color = R.color.purple_700;
    public static int lastcolor;
    public static int theme;

    public static DrawerLayout drawer;
    public static NavController navController;

    public static int flag = 0;
    public static boolean cambiar = false;

    public static Activity reload;

    public static Thread hilo;
    public static Runnable runnable;
    public static AdapterPers adapter;
    public static ArrayList<Hilo> arrayHilos;

    public static Database db;

    public static boolean traigoUrl = false;
    public static String urlTraida = "";

    public static Button homeButton;
    public static MenuInflater inflater;

    public static String cookies;

    private static Context context;

    public static String nombre_hilo_actualizado;

    private static int colorAzul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //reload = MainActivity.this;

        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        darkmode = preferences.getBoolean("darkmode", false);
        keepPage = preferences.getBoolean("keepPage", false);
        urlToLoad = preferences.getString("urlToLoad", HomeFragment.mainUrl);

        inflater = getMenuInflater();

        colorAzul = getResources().getColor(R.color.blue_500);

        ///////////////

        db = new Database(getApplicationContext());

        ///////////////

        MainActivity.context = getApplicationContext();

        //arrayHilos.add(new Hilo("Forococccce","holaa"));

        Log.i("Flag: ", String.valueOf(flag));

        super.onCreate(savedInstanceState);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        dialog = new AlertDialog.Builder( MainActivity.this);
        view = getLayoutInflater().inflate( R.layout.colorpicker, null);

        //final AlertDialog alertDialog;
        //builder = new AlertDialog.Builder(MainActivity.this);

        Log.i("tags", "Empieza la app");

        //setTheme(R.style.Theme_ForocochesAdBlock);
        //color = R.color.blue_700;

        Log.i("tags", "Url que en teoria se carga: "+urlToLoad);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_favoritos, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.copylink) {
            String url = HomeFragment.web.getUrl();
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Fc url", url);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(MainActivity.this, "URL copiada al portapapeles", Toast.LENGTH_SHORT).show();
        }else if(item.getItemId() == R.id.sharewapp){

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            String url = HomeFragment.web.getUrl();
            String title = HomeFragment.web.getTitle();
            intent.putExtra(Intent.EXTRA_TEXT, title + "\n" + url);

            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                ex.printStackTrace();

                Toast.makeText(MainActivity.this, "El dispositivo no tiene instalado WhatsApp", Toast.LENGTH_SHORT).show();
            }

        }else if(item.getItemId() == R.id.reloadPage){

            String url = HomeFragment.web.getUrl();
            HomeFragment.web.loadUrl(url);

        }
        else if(item.getItemId() == R.id.addfavourite){

            Toast.makeText(MainActivity.this, "Hilo añadido a favoritos", Toast.LENGTH_SHORT).show();
            Database db = new Database(getApplicationContext());
            db.agregarHilo();

        }
        else{

            Log.i("Flag Linear", String.valueOf(flag));

            LinearLayout lateralDrawer = findViewById(R.id.lateralDrawer);
            lateralDrawer.setBackgroundColor(getResources().getColor(R.color.blue_500));

            //cambiar =false;

            Log.i("tema", String.valueOf(theme));

            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                    || super.onSupportNavigateUp();
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        CookieManager.getInstance().flush();

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("darkmode", darkmode);
        editor.putBoolean("keepPage", keepPage);

        if(HomeFragment.keepPage == true){
            lastUrl = HomeFragment.web.getUrl();
            Log.i("tags", "url guardada: "+lastUrl);
        }else{
            lastUrl = HomeFragment.mainUrl;
            Log.i("tags", "url guardada: "+lastUrl);
        }

        editor.putString("urlToLoad", lastUrl);
        //editor.putInt("theme", theme);
        //editor.putInt("color", color);
        //editor.putBoolean("cambiar", cambiar);

        editor.commit();

        Log.i("tags", "Pausa de la app (Se guardan los datos)");

    }

//    @Override
//    protected void onStop() {
//
//        //SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
//        //SharedPreferences.Editor editor = sharedPreferences.edit();
//        //editor.putInt("flag", flag);
//        //editor.commit();
//
//        super.onStop();
//    }

    @Override
    public void onBackPressed() {
        if (HomeFragment.web.canGoBack()) {
            HomeFragment.web.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if(darkmode == true){
            outState.putBoolean("darkmode", true);
        }else{
            outState.putBoolean("darkmode", false);
        }
        Log.i("tags", "2");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) { //Esto creo que no va
        super.onRestoreInstanceState(savedInstanceState);
        darkmode = savedInstanceState.getBoolean("darkmode");
        keepPage = savedInstanceState.getBoolean("keepPage");
        if(darkmode == true){
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
            HomeFragment.web.reload();
        }else{
            WebSettingsCompat.setForceDark(HomeFragment.web.getSettings(), WebSettingsCompat.FORCE_DARK_OFF);
            HomeFragment.web.reload();
        }

        if(keepPage == true){
            lastUrl = HomeFragment.web.getUrl();
            HomeFragment.web.loadUrl(lastUrl);
        }else{
            HomeFragment.web.loadUrl(HomeFragment.mainUrl);
        }

        Log.i("tags", "3");

    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    public static void lanzarNoti(){

        Context c = getAppContext();

        /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getAppContext());
        mBuilder.setSmallIcon(R.drawable.iconoforo2);
        mBuilder.setContentTitle("Notification Alert, Click Me!");
        mBuilder.setContentText("Hi, This is Android Notification Detail!");
        NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());*/

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getAppContext());

        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription("Description");
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        //mChannel.enableVibration(true);
        //mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mChannel.setShowBadge(false);

        notificationManager.createNotificationChannel(mChannel);

        //Intent intent = new Intent(getAppContext(), MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //PendingIntent pendingIntent = PendingIntent.getActivity(getAppContext(), 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getAppContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.roto2)
                .setContentTitle("Nueva actualización en hilo")
                .setContentText(MainActivity.nombre_hilo_actualizado)
                .setColor(colorAzul)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                //.setContentIntent(pendingIntent);

        // notificationId is a unique int for each notification that you must define
        int notificationId = 1;
        notificationManager.notify(notificationId, builder.build());

    }

    /*public class doit extends AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {

            //checkAllUrls();

            try {
                Document doc = Jsoup.connect("dfsaf").get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }*/

//    public static void reloadList(){
//
//        ArrayList<Hilo> arrayHilos = db.readAll();
//
//        adapter = new AdapterPers(this, arrayHilos);
//        // Attach the adapter to a ListView
//        listView.setAdapter(adapter);
//
//    }

}